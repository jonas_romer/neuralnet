/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

import org.jblas.DoubleMatrix;

/**
 *
 * @author Jonas
 */
public interface Layer {

    ActivationFunction getActivation();

    Result forward(DoubleMatrix in);

    DoubleMatrix getWeights();

    DoubleMatrix getBias();

    void updateWeights(DoubleMatrix deltas);

    void updateBias(DoubleMatrix deltas);

}
