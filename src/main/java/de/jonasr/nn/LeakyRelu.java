/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

/**
 *
 * @author Jonas
 */
public class LeakyRelu implements ActivationFunction {

    private final double alpha;

    public LeakyRelu(double alpha) {
        this.alpha = alpha;
    }

    public LeakyRelu() {
        this.alpha = 0.1;
    }

    @Override
    public double f(double x) {
        if (x < 0.0) {
            return alpha * x;
        }
        return x;
    }

    @Override
    public double df(double x) {
        if (x <= 0.0) {
            return alpha;
        }
        return 1.0;
    }

}
