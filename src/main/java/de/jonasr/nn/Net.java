/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Jonas
 */
public class Net {

    private ArrayList<Layer> layers = new ArrayList<>();

    public Net(int... neurons) {
        var sigmoid = new Sigmoid();
        var relu = new SmoothReluApproximation();

        layers.add(new InputLayer(neurons[0], sigmoid));

        for (int i = 1; i < neurons.length; i++) {
            ActivationFunction a = (i == neurons.length - 1 ? sigmoid : relu);

            layers.add(new DenseLayer(neurons[i - 1], neurons[i], a));
        }
    }

    public DoubleMatrix forward(DoubleMatrix input) {
        var output = input;
        for (var l : layers) {
            output = l.forward(output).out;
        }
        return output;
    }

    public void learn(List<Sample> samples, int batchSize, double learnRate) {
        var weightGradients = new ArrayList<DoubleMatrix>(layers.size());
        var biasGradients = new ArrayList<DoubleMatrix>(layers.size());

        for (var l : layers) {
            weightGradients.add(zeroCopy(l.getWeights()));
            biasGradients.add(zeroCopy(l.getBias()));
        }

        int sampleCounter = 0;

        for (Sample sample : samples) {
            sampleCounter++;

            var forwardPass = new ArrayList<Result>(layers.size());

            // forward pass
            var nextInput = sample.input;
            for (var l : layers) {
                Result result = l.forward(nextInput);
                forwardPass.add(result);
                nextInput = result.out;
            }

            var lastIndex = layers.size() - 1;
            DoubleMatrix deltas = null;

            // backpropagate
            for (int i = lastIndex; i > 0; i--) {
                Layer layer = layers.get(i);
                Result result = forwardPass.get(i);
                ActivationFunction a = layer.getActivation();
                
                // dPhi(net)
                var dPhi = apply(result.net.dup(), v -> a.df(v));

                // compute deltas
                if (i == lastIndex) {
                    // output layer
                    deltas = result.out.sub(sample.target).mul(dPhi);
                } else {
                    // hidden layer
                    var nextLayer = layers.get(i + 1);
                    deltas = deltas.transpose().mmul(nextLayer.getWeights()).transpose().mul(dPhi);
                }

                var prevOut = forwardPass.get(i - 1).out;

                final DoubleMatrix weightGradient = deltas.mmul(prevOut.transpose()).mul(-learnRate);
                weightGradients.get(i).addi(weightGradient);

                final DoubleMatrix biasGradient = deltas.mul(-learnRate);
                biasGradients.get(i).addi(biasGradient);
            }

            if (sampleCounter == batchSize) {
                // commit batch gradients
                for (int i = 1; i < layers.size(); i++) {
                    layers.get(i).updateWeights(weightGradients.get(i).div(batchSize));
                    layers.get(i).updateBias(biasGradients.get(i).div(batchSize));
                }

                // reset
                sampleCounter = 0;
                weightGradients.forEach(m -> {
                    Arrays.fill(m.data, 0);
                });
                biasGradients.forEach(m -> {
                    Arrays.fill(m.data, 0);
                });
            }
        }
    }

    public void learnBatch(List<Sample> learnData, double learnRate) {
        learn(learnData, learnData.size(), learnRate);
    }

    public void learnAll(List<Sample> learnData, double learnRate) {
        learn(learnData, 1, learnRate);
    }

    public void learnSingle(Sample learnData, double learnRate) {
        learn(List.of(learnData), 1, learnRate);
    }

    private static <T> T last(List<T> l) {
        return l.get(l.size() - 1);
    }

    private static DoubleMatrix zeroCopy(DoubleMatrix m) {
        return new DoubleMatrix(m.rows, m.columns);
    }

    // apply function in place
    private static DoubleMatrix apply(DoubleMatrix m, Function<Double, Double> transform) {
        for (int i = 0; i < m.data.length; i++) {
            m.data[i] = transform.apply(m.data[i]);
        }
        return m;
    }

}
