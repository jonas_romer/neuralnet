/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

import java.util.Arrays;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Jonas
 */
public class InputLayer implements Layer {

    private final ActivationFunction activation;
    private final DoubleMatrix weights;
    private final DoubleMatrix bias;

    public InputLayer(int inputs, ActivationFunction activation) {
        this.activation = activation;

        weights = DoubleMatrix.ones(1, inputs);
        bias = DoubleMatrix.zeros(inputs);
    }

    @Override
    public ActivationFunction getActivation() {
        return activation;
    }

    @Override
    public Result forward(DoubleMatrix in) {
        if (in.length != weights.length) {
            throw new IllegalArgumentException("Input data doesn't match number of input neurons");
        }

        var net = in.dup();
        var out = net.dup();

        Arrays.setAll(out.data, i -> {
            return activation.f(out.data[i]);
        });

        return new Result(net, out);
    }

    @Override
    public DoubleMatrix getWeights() {
        return weights;
    }

    @Override
    public DoubleMatrix getBias() {
        return bias;
    }

    @Override
    public void updateWeights(DoubleMatrix deltas) {
        // not supported
    }

    @Override
    public void updateBias(DoubleMatrix deltas) {
        // not supported
    }

}
