/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

import java.util.Arrays;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Jonas
 */
public class DenseLayer implements Layer {

    private DoubleMatrix weights;
    private DoubleMatrix bias;
    private ActivationFunction activation;

    public DenseLayer(int inputs, int neurons, ActivationFunction activation) {
        weights = new DoubleMatrix(neurons, inputs);
        bias = new DoubleMatrix(neurons);

        this.activation = activation;

        randomize();
    }

    public final void randomize() {
        Arrays.setAll(weights.data, i -> {
            return Math.random();
        });
        Arrays.setAll(bias.data, i -> {
            return Math.random();
        });
    }

    @Override
    public Result forward(DoubleMatrix input) {
        var net = weights.mmul(input).add(bias);

        var out = new DoubleMatrix(net.getLength());
        Arrays.setAll(out.data, i -> {
            return activation.f(net.data[i]);
        });

        return new Result(net, out);
    }

    @Override
    public ActivationFunction getActivation() {
        return activation;
    }

    @Override
    public DoubleMatrix getWeights() {
        return weights;
    }

    @Override
    public DoubleMatrix getBias() {
        return bias;
    }

    @Override
    public void updateWeights(DoubleMatrix weightDeltas) {
        weights.addi(weightDeltas);
    }

    @Override
    public void updateBias(DoubleMatrix biasDeltas) {
        bias.addi(biasDeltas);
    }
}
