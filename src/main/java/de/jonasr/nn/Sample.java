/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

import org.jblas.DoubleMatrix;

/**
 *
 * @author Jonas
 */
public class Sample {

    public final DoubleMatrix input;
    public final DoubleMatrix target;

    public Sample(DoubleMatrix input, DoubleMatrix target) {
        this.input = input;
        this.target = target;
    }

    public Sample(double[] input, double[] target) {
        this.input = new DoubleMatrix(input);
        this.target = new DoubleMatrix(target);
    }

}
