/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

/**
 *
 * @author Jonas
 */
public class Relu implements ActivationFunction {

    @Override
    public double f(double x) {
        if (x < 0.0) {
            return 0.0;
        }
        return x;
    }

    @Override
    public double df(double x) {
        if (x <= 0.0) {
            return 0.0;
        }
        return 1.0;
    }

}
