/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

/**
 *
 * @author Jonas
 */
public class SmoothReluApproximation implements ActivationFunction {

    @Override
    public double f(double x) {
        return Math.log(1.0 + Math.exp(x));
    }

    @Override
    public double df(double x) {
        final double expX = Math.exp(x);
        return expX / (1.0 + expX);
    }
    
}
