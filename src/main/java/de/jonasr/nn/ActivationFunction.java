/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

/**
 *
 * @author Jonas
 */
public interface ActivationFunction {

    double f(double x);

    double df(double x);
}
