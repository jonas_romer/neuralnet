/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

import org.jblas.DoubleMatrix;

/**
 *
 * @author Jonas
 */
public class Result {
    
    public final DoubleMatrix net;
    public final DoubleMatrix out;

    public Result(DoubleMatrix net, DoubleMatrix out) {
        this.net = net;
        this.out = out;
    }
    
}
