/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

/**
 *
 * @author Jonas
 */
public class Sigmoid implements ActivationFunction {

    @Override
    public double f(double x) {
        return 1.0 / (1.0 + Math.exp(-x));
    }

    @Override
    public double df(double x) {
        double tmp = f(x);
        return tmp * (1 - tmp);
    }

}
