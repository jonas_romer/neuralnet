/*
 * License: Do whatever you want
 */
package de.jonasr.nn;

import java.util.List;

/**
 *
 * @author Jonas
 */
public class Main {

    public static void main(String[] args) {
        Net nn = new Net(2, 4, 1);

        var samples = List.of(
                new Sample(new double[]{0, 0}, new double[]{0}),
                new Sample(new double[]{0, 1}, new double[]{1}),
                new Sample(new double[]{1, 0}, new double[]{1}),
                new Sample(new double[]{1, 1}, new double[]{0})
        );

        for (int i = 0; i < 100000; i++) {
            nn.learnAll(samples, 0.2);
        }

        for (Sample sample : samples) {
            var o = nn.forward(sample.input);
            System.out.print(sample.input.toString());
            System.out.print(" -> ");
            System.out.print(o.toString());
            System.out.print(" ");
            System.out.print(o.sub(sample.target));
            System.out.println();
        }
        System.out.println();
    }
}
