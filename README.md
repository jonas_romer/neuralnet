# A very simple neural net implementation

This is a minimal neural net and backpropagation / gradient descent implementation with only jBlas as dependency.

The code is just for fun and education. If you find it useful use it however you want. Please let me know if you find bugs.
